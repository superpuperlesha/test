<section class="contacts">
	<div class="container">
		<div class="row">
			<div class="contacts__description">
				<h2><?php the_field('contact_title') ?></h2>
				<?php the_field('contact_content') ?>
			</div>
			<div class="contacts__form">
				<?php echo do_shortcode(get_field('contact_formcode')) ?>
			</div>
		</div>
	</div>
</section>