<section class="contacts">
	<div class="container">
		<div class="contacts__description">
			<h2><?php the_field('lthp_title') ?></h2>
		</div>
		<div class="row"><?php
			$lastposts = get_posts(array('posts_per_page'=>get_field('lthp_count')));
			foreach($lastposts as $post){
				setup_postdata($post);
				get_template_part('template_part/list_item');
			}
			wp_reset_postdata(); ?>
		</div>
	</div>
</section>