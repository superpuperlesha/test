<section class="contacts">
	<div class="container">
		<div class="contacts__description">
			<h2><?php the_field('popp_title') ?></h2>
		</div>
		<div class="row"><?php
			$arg = array(
					'posts_per_page' => get_field('popp_count'),
					'meta_key'       => 'sp_view',
					'orderby'        => 'meta_value_num',
					'order'          => 'DESC'
				);
			$lastposts = get_posts($arg);
			foreach($lastposts as $post){
				setup_postdata($post);
				get_template_part('template_part/list_item_v1');
			}
			wp_reset_postdata(); ?>
		</div>
	</div>
</section>