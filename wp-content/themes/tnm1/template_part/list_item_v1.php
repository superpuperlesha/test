<div <?php post_class('col-12 col-xl-6') ?> id="post-<?php the_ID() ?>">
	<div class="my-blog__block">
		<div class="csm100">
			<a href="<?php the_permalink() ?>">
				<h3><?php the_title() ?></h3>
			</a>
			<div class="text-center">
				<?php echo __('Views', 'tnm').': '.get_post_meta(get_the_id(), 'sp_view', true) ?>
			</div>
		</div>
		<div class="csm100">
			<?php echo get_excerpt_v1() ?>
			<p class="postTime">
				<a href="<?php echo get_the_permalink() ?>"><?php _e('Read more', 'tnm') ?></a>
			</p>
		</div>
	</div>
</div>