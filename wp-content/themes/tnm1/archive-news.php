<?php get_header() ?>

<section class="container simplePage woocommerce-account">
	<div class="row">
		<div class="col">
			<h1><?php _e('Archive news', 'tnm') ?></h1>
			<div class="woocommerce"><?php
				do_action('woocommerce_account_navigation'); ?>
				<div class="woocommerce-MyAccount-content"><?php
					if( is_user_logged_in() && (int)get_user_meta(get_current_user_id(), 'tnm_mailcount', true)>1 ){
						if(have_posts()){
							while(have_posts()){
								the_post();
								get_template_part('template_part/woo_news_item');
							}
							get_template_part('template_part/pager');
						}else{
							get_template_part('template_part/not_found');
						}
					}else{
						echo '<h3>'.__('You must be registered!', 'tnm').'</h3>';
						echo '<h3>'.__('You must have subscription!', 'tnm').'</h3>';
					} ?>
				</div>
			</div>
		</div>
	</div>
</section>

<?php get_footer() ?>