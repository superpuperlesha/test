<?php
/*
Template Name: Home
*/
get_header(); ?>

<?php while(have_posts()){
	the_post(); ?>
	
	<?php get_template_part('template_part/block_lthp') ?>
	
	<?php get_template_part('template_part/block_poppost') ?>
	
	<?php get_template_part('template_part/block_contacts') ?>
	
<?php } ?>
	
<?php get_footer() ?>