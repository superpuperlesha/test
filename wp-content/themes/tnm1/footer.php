	</main>
	
	<footer class="footer">
		<div class="container-fluid">
			<div class="row">
				<div class="col-12 col-md-3 col-lg-3 col-xl-3">
					<a href="<?php echo home_url() ?>" class="footer__logo">
						<?php bloginfo('name') ?>
					</a>
					<div class="footer__copyright">
						<?php the_field('sopt_copy', 'option') ?>
					</div>
				</div>
				<div class="col-12 col-md-3 col-lg-3 col-xl-3">
					<div class="footer__contacts">
						<p><?php the_field('sopt_foot_widg_title', 'option') ?></p>
						<div class="some">
							<?php the_field('sopt_foot_widg_content_1', 'option') ?>
						</div>
					</div>
				</div>
				<div class="col-12 col-md-3 col-lg-3 col-xl-3">
					<div class="footer__contacts">
						<div class="some">
							<?php the_field('sopt_foot_widg_content_2', 'option') ?>
						</div>						
					</div>
				</div>
				<div class="col-12 col-md-3 col-lg-3 col-xl-3">
					<div class="footer__copyright">
						<?php the_field('sopt_copy', 'option') ?>
						<div>
							<?php echo get_theme_mod( 'mytheme_company-name' ) ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>
	
	<div id="CMSScripts">
		<script>
			var WPThemeURL = '<?php echo get_template_directory_uri(); ?>/';
			var WPajaxURL = '<?php echo admin_url('admin-ajax.php'); ?>';
		</script>
		<?php wp_footer() ?>
	</div>
	
</body>
</html>