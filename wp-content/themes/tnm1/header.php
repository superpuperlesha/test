<!DOCTYPE html>
<html <?php language_attributes() ?>>
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<meta charset="<?php bloginfo('charset') ?>">
		<?php wp_head() ?>
	</head>
	<body <?php body_class() ?>>
		<header class="header" role="banner">
			<div class="container-fluid">
				<div class="row">
					<div class="col-4 d-flex align-items-center">
						<a href="<?php echo home_url() ?>" class="header__logo">
							<?php bloginfo('name') ?>
						</a>
					</div>
					<div class="col-8 d-flex justify-content-end align-items-center">
						<nav class="header__nav">
							<?php if( has_nav_menu('HeaderMenu')){
								wp_nav_menu( array(
										'container'      => false,
										'theme_location' => 'HeaderMenu',
										'menu_id'        => 'HeaderMenu_ID_0',
										'menu_class'     => 'header__nav-inner',
										'items_wrap'     => '<ul id="%1$s" class="%2$s">%3$s</ul>',
										'walker'         => new CWalker_Menu(),
									)
								);
							} ?>
						</nav>
						<div class="header__nav-hamburger">
							<span></span>
							<span></span>
							<span></span>
							<span></span>
						</div>
					</div>
				</div>
			</div>
		</header>
		
		<main role="main">