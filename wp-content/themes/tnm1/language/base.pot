#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Base\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-23 09:54+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: \n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-KeywordsList: _;gettext;gettext_noop;__;_e\n"
"X-Poedit-Basepath: .\n"
"X-Poedit-SearchPath-0: ..\n"
"Language: \n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"X-Generator: Loco https://localise.biz/"

#: searchform.php:1
msgid "Enter search terms&hellip;"
msgstr ""

#: searchform.php:5 inc/default.php:98
msgid "Search"
msgstr ""

#: comments.php:8
msgid "This post is password protected. Enter the password to view comments."
msgstr ""

#: comments.php:19
msgid "(Edit)"
msgstr ""

#: comments.php:20
msgid "at"
msgstr ""

#: comments.php:20
msgid "said:"
msgstr ""

#: comments.php:22
msgid "Your comment is awaiting moderation."
msgstr ""

#: comments.php:29
msgid "Reply"
msgstr ""

#: comments.php:46
msgid "No Responses"
msgstr ""

#: comments.php:46
msgid "One Response"
msgstr ""

#: comments.php:46
msgid "% Responses"
msgstr ""

#: comments.php:46
msgid "to"
msgstr ""

#: comments.php:68
msgid "Comments are closed."
msgstr ""

#: template_part/pager.php:6
msgid "Previous page"
msgstr ""

#: template_part/pager.php:7
msgid "Next page"
msgstr ""

#: template_part/list_item.php:14
msgid "by"
msgstr ""

#: template_part/list_item.php:28
msgid "Posted in"
msgstr ""

#: template_part/list_item.php:29
msgid "No Comments"
msgstr ""

#: template_part/list_item.php:29
msgid "1 Comment"
msgstr ""

#: template_part/list_item.php:29
msgid "% Comments"
msgstr ""

#: template_part/list_item.php:30
msgid "<li>Tags: "
msgstr ""

#: template_part/list_item.php:31
msgid "Edit"
msgstr ""

#: template_part/not_found.php:3
msgid "Not Found"
msgstr ""

#: template_part/not_found.php:6
msgid "Sorry, but you are looking for something that isn't here."
msgstr ""

#: inc/default.php:7
#, php-format
msgid ""
"You are blocking access to robots. You must go to your <a href=\"%s\">"
"Reading</a> settings and uncheck the box for Search Engine Visibility."
msgstr ""

#: inc/default.php:150
msgid ""
"This content is password protected. To view it please enter your password "
"below:"
msgstr ""

#: inc/default.php:151
msgid "Password:"
msgstr ""

#: inc/default.php:151
msgid "Submit"
msgstr ""

#: inc/default.php:205
msgid "Date archive link"
msgstr ""

#: inc/default.php:212
msgid "Type"
msgstr ""

#: inc/default.php:227
msgid "Day"
msgstr ""

#: inc/default.php:228
msgid "Month"
msgstr ""

#: inc/default.php:229
msgid "Year"
msgstr ""

#: inc/menus.php:5
msgid "Header menu"
msgstr ""

#: inc/menus.php:6
msgid "Footer menu"
msgstr ""

#. Name of the template
msgid "Home"
msgstr ""

#. Name of the template
msgid "Contact"
msgstr ""

#. Name of the theme
msgid "base"
msgstr ""

#. Description of the theme
msgid "base theme for Wordpress"
msgstr ""

#. Author of the theme
msgid "apri-code"
msgstr ""

#. Author URI of the theme
msgid "https://apri-code.com/"
msgstr ""
