<?php get_header(); ?>

<?php while(have_posts()){
	the_post(); ?>
	<section class="container simplePage">
		<div class="row">
			<div class="col">
				<h1><?php the_title() ?></h1>
				<?php if( is_user_logged_in() && (int)get_user_meta(get_current_user_id(), 'tnm_mailcount', true)>1 ){
					the_content();
					
					if(comments_open()){
						comments_template();
					}
				}else{
					echo '<h3>'.__('You must be registered!', 'tnm').'</h3>';
					echo '<h3>'.__('You must have subscription!', 'tnm').'</h3>';
				} ?>
			</div>
		</div>
	</section>
<?php } ?>


<script>
	//===add view for post===
	var addViewPost = <?php the_id() ?>;
	//alert(<?php echo (int)get_post_meta(get_the_id(), 'sp_view', true) ?>);
</script>

<?php get_footer(); ?>