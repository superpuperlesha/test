//$(document).ready(function(){
jQuery(document).ready(function($){
	
	//===add view for post===
	if(typeof addViewPost != 'undefined'){
		
		$.ajax({
			url:         WPajaxURL,
			type:        'POST',
			contentType: 'application/json; charset=utf-8',
			dataType:    'json',
			data: {
				action:        'addViewPost',
				'addViewPost': addViewPost
			},
			success: function(data){
				
			},
			error: function (errormessage){
				//console.log(errormessage);
            }
		});
		
	}
	
	//===CF& on message===
	var wpcf7Form = document.querySelector( '.wpcf7' );
	wpcf7Form.addEventListener( 'wpcf7submit', function( event ) {
		alert('JavaScript submit fire...');
	}, false );
	
});