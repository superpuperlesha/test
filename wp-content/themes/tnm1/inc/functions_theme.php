<?php

//===get fist X words from post===
function get_excerpt_v1(){
	$excerpt = get_the_content();
	$excerpt = preg_replace(" ([.*?])",'',$excerpt);
	$excerpt = strip_shortcodes($excerpt);
	$excerpt = strip_tags($excerpt);
	$excerpt = substr($excerpt, 0, 100);
	$excerpt = substr($excerpt, 0, strripos($excerpt, " "));
	$excerpt = trim(preg_replace( '/\s+/', ' ', $excerpt));
	return $excerpt;
}

function custom_excerpt_length( $length ) {
	return 30;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

//===add filter for excerpt (qtranslatex)===
add_filter('get_the_excerpt', 'filter_function_name_2980', 10, 2);
function filter_function_name_2980($post_excerpt, $post){
	$post_excerpt = apply_filters('the_excerpt', $post_excerpt);
	return $post_excerpt;
}


//==BOOTSTRAPE 4 Page Navigation===
function page_navicx($before = '', $after = ''){
    global $wpdb, $wp_query;
    $request = $wp_query->request;
    $posts_per_page = intval(get_query_var('posts_per_page'));
    $paged = intval(get_query_var('paged'));
    $numposts = $wp_query->found_posts;
    $max_page = $wp_query->max_num_pages;
    if ( $numposts <= $posts_per_page ) { return; }
    if(empty($paged) || $paged == 0) {
        $paged = 1;
    }
    $pages_to_show = 7;
    $pages_to_show_minus_1 = $pages_to_show-1;
    $half_page_start = floor($pages_to_show_minus_1/2);
    $half_page_end = ceil($pages_to_show_minus_1/2);
    $start_page = $paged - $half_page_start;
    if($start_page <= 0) {
        $start_page = 1;
    }
    $end_page = $paged + $half_page_end;
    if(($end_page - $start_page) != $pages_to_show_minus_1) {
        $end_page = $start_page + $pages_to_show_minus_1;
    }
    if($end_page > $max_page) {
        $start_page = $max_page - $pages_to_show_minus_1;
        $end_page = $max_page;
    }
    if($start_page <= 0) {
        $start_page = 1;
    }
	
    echo $before.'<ul class="pagination list-unstyled">';
    // if($paged > 1){
        // echo '<li class="page-item prev">
				// <a class="page-link" href="'.get_pagenum_link().'" title="First"><span></span><span></span><span></span></a>
			  // </li>';
    // }
	
    if(get_previous_posts_link()){
		echo '<li class="page-item">
				<a class="page-link btn btn-slick btn-prev" href="'.get_previous_posts_page_link().'"><span></span><span></span><span></span></a>
			  </li>'; 
	}else{
		echo '<li class="page-item disabled">
				<a class="page-link btn btn-slick btn-prev" href="#"><span></span><span></span><span></span></a>
			  </li>';
	}
     
    for($i = $start_page; $i <= $end_page; $i++){
        if($i == $paged){
            echo '<li class="page-item active">
					<a class="page-link" href="#">'.$i.'</a>
				  </li>';
        }else{
            echo '<li class="page-item">
					<a class="page-link" href="'.get_pagenum_link($i).'">'.$i.'</a>
				  </li>';
        }
    }
	
    if(get_next_posts_link()){
		echo '<li class="page-item">
				<a class="page-link btn btn-slick btn-next" href="'.get_next_posts_page_link().'"><span></span><span></span><span></span></a>
			  </li>';
	}else{
		echo '<li class="page-item disabled">
				<a class="page-link btn btn-slick btn-next" href="#"><span></span><span></span><span></span></a>
			  </li>';
	}
	
    // if($end_page < $max_page) {
        // $last_page_text = '<span></span><span></span><span></span>';
        // echo '<li class="page-item next"><a class="page-link" href="'.get_pagenum_link($max_page).'" title="Last">'.$last_page_text.'</a></li>';
    // }
    echo '</ul>'.$after;
}

