<?php

function mytheme_customize_register( $wp_customize ) {

    $wp_customize->add_section( 'mytheme_company_section' , array(
        'title'      => __( 'Additional Company Info', 'mytheme' ),
        'priority'   => 30,
    ));

    $wp_customize->add_setting( 'mytheme_company-name', array());
    $wp_customize->add_control( new WP_Customize_Control(
        $wp_customize,
        'mytheme_company_control',
            array(
                'label'      => __( 'Company Name', 'mytheme' ),
                'section'    => 'mytheme_company_section',
                'settings'   => 'mytheme_company-name',
                'priority'   => 1
            )
        )
    );
	
}
add_action( 'customize_register', 'mytheme_customize_register' );



