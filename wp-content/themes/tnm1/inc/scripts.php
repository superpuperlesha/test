<?php
// Theme css & js
function base_scripts_styles() {
	$in_footer = true;
	
	wp_deregister_script('jquery');
	//wp_enqueue_script('jquery',              get_template_directory_uri().'/js/jquery-3.3.1.min.js',                      array(),         '', $in_footer);
	wp_enqueue_script('jquery',              get_template_directory_uri().'/_slicing/data/js/jquery-1.11.1.min.js',                      array(),         '', $in_footer);
}
add_action('wp_enqueue_scripts', 'base_scripts_styles');

//===Theme css & js IN FOOTER===
function prefix_add_footer_styles(){
	$in_footer = true;
	
	wp_enqueue_style('main',                 get_template_directory_uri().'/_slicing/data/css/main.css',      array());
	wp_enqueue_style('theme',                get_stylesheet_uri(),                                            array());
	
	wp_enqueue_script('main',               get_template_directory_uri().'/_slicing/data/js/main.js',        array('jquery'), '', $in_footer);
	
	//===GOOGLE JS TAGMANAGER ANALITIC===
	// $googletagsData = get_transient('googletagsData');
	// if(false === $googletagsData){
		// $googletagsData = file_get_contents('https://www.googletagmanager.com/gtag/js?id=UA-104189313-1');
		// $googletagsData = $googletagsData.' window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag(\'js\', new Date()); gtag(\'config\', \'UA-104189313-1\'); ';
		// $file = fopen(get_template_directory().'/googletagsData.js', 'w');
		// fputs($file, $googletagsData);
		// fclose($file);
		// set_transient('googletagsData', $googletagsData, 86400);
	// }
	// wp_enqueue_script('googletagsData', get_template_directory_uri().'/googletagsData.js',                                          array('jquery'), '', $in_footer);
	
	wp_enqueue_script('comment-reply');
};
add_action('get_footer', 'prefix_add_footer_styles');

function wpse71503_init() {
    if (!is_admin()) {
        wp_deregister_style('thickbox');
        wp_deregister_script('thickbox');
    }
}
add_action('init', 'wpse71503_init');

//=====GOOGLE SPEED TEST=====
function remove_cssjs_ver( $src ){
	if( strpos( $src, '?ver=' ) )
		$src = remove_query_arg( 'ver', $src );
	return $src;
}
add_filter( 'style_loader_src',  'remove_cssjs_ver', 10, 2 );
add_filter( 'script_loader_src', 'remove_cssjs_ver', 10, 2 );
//=====//GOOGLE SPEED TEST=====

//===HTML VALIDATION===
add_filter('style_loader_tag', 'clean_style_tag');
function clean_style_tag($src) {
	$src = str_replace("type='text/css'", '', $src);
	$src = str_replace('type="text/css"', '', $src);
    return $src;
}
add_filter('script_loader_tag', 'clean_script_tag');
function clean_script_tag($src) {
	$src = str_replace("type='text/javascript'", '', $src);
	$src = str_replace('type="text/javascript"', '', $src);
    return $src;
}
//===//HTML VALIDATION===

//===ACF QTANSLATE admin styles===
add_action('admin_head', 'my_custom_fonts');
function my_custom_fonts() {
	echo'<style>
		.multi-language-field {
			margin-top: -20px;
		}
	</style>';
}
