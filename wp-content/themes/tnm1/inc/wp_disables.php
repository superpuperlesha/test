<?php
	//===Disable Emoji===
	remove_action('wp_head',             'wp_generator');
	remove_action('wp_head',             'print_emoji_detection_script', 7);
	remove_action('admin_print_scripts', 'print_emoji_detection_script');
	remove_action('wp_print_styles',     'print_emoji_styles');
	remove_action('admin_print_styles',  'print_emoji_styles');
	remove_filter('the_content_feed',    'wp_staticize_emoji');
	remove_filter('comment_text_rss',    'wp_staticize_emoji');
	remove_filter('wp_mail',             'wp_staticize_emoji_for_email');
	
	//===Disable embeds on init===
	add_action('init', function(){
		remove_action('wp_head',          'wp_oembed_add_discovery_links');
		remove_action('wp_head',          'wp_oembed_add_host_js');
	}, PHP_INT_MAX - 1);
	
	//===disable NEW EDITOR for posts===
	add_filter('use_block_editor_for_post', '__return_false', 10);
	//===disable NEW EDITOR for post types===
	add_filter('use_block_editor_for_post_type', '__return_false', 10);
	
	//===remove defoult editor from pages===
	add_action('admin_init', 'hide_editor');
	function hide_editor(){
		if(isset($_POST['post_ID'])){$post_id = $_POST['post_ID'];}
		if(isset($_GET['post'])){$post_id = $_GET['post'];}
		if(!isset($post_id))return;
		$tpn = get_page_template_slug($post_id);
		if( $tpn == 'template_page/home.php' ){
			remove_post_type_support('page', 'editor');
			remove_post_type_support('page', 'thumbnail');
		}
	}
	
?>